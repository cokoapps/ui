# Coko Apps UI

This is a repo for common components to be used with different apps developed by Coko.

It uses Storybook as a development environment.

## Get started

Clone the repo.

The, install the dependencies:

```
yarn
```

Now you can run storybook:

```
yarn storybook
```

Storybook has already been set up:
* to transpile `@pubsweet/ui` (which doesn't come transpiled)
* to use `@pubsweet/coko-theme`

module.exports = {
  moduleNameMapper: {
    'typeface-': '<rootDir>/styleMock.js',
  },
  transformIgnorePatterns: [
    'node_modules/(?!(@pubsweet/ui|@pubsweet/coko-theme|@pubsweet/ui-toolkit)/)',
  ],
}

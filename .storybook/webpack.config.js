const path = require('path')

module.exports = async ({ config }) => {
  if (config.name !== 'manager') {
    const jsLoader = config.module.rules.find(
      rule => rule.use[0].loader === 'babel-loader',
    )

    const UILocation = path.join(__dirname, '..', 'src')

    const pubsweetUILocation = path.join(
      __dirname,
      '..',
      'node_modules',
      '@pubsweet',
      'ui',
    )

    jsLoader.include = [UILocation, pubsweetUILocation]
    jsLoader.exclude = []
  }

  return config
}

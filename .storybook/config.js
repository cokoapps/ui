import { addDecorator, configure } from '@storybook/react'
import { withA11y } from '@storybook/addon-a11y'
import { withKnobs } from '@storybook/addon-knobs'
// import { withSmartKnobs } from 'storybook-addon-smart-knobs'
import { withThemesProvider } from 'storybook-addon-styled-component-theme'

import cokoTheme from '@pubsweet/coko-theme'

// TODO: Should be moved to theme's repo
cokoTheme.name = 'Coko theme'
const themes = [cokoTheme]

configure(require.context('../src/stories', true, /\.stories\.js$/), module)

addDecorator(withA11y)
// addDecorator(withSmartKnobs)
addDecorator(withKnobs)
addDecorator(withThemesProvider(themes))

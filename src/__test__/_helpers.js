import React from 'react'
import renderer from 'react-test-renderer'
import { render as reactRender } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'

import theme from '@pubsweet/coko-theme'

const themed = node => <ThemeProvider theme={theme}>{node}</ThemeProvider>

const create = node => renderer.create(themed(node))
const render = node => reactRender(themed(node))

export { create, render }

import React from 'react'
import 'jest-styled-components'
import { Action, ActionGroup } from '@pubsweet/ui'
import styled from 'styled-components'

import { SectionItem } from '../ui'

import { create, render } from './_helpers'

const items = [{ id: 1, text: 'Edit' }, { id: 2, text: 'Delete' }]

const actionComponent = (
  <ActionGroup>
    {items.map(item => (
      <Action
        data-testid="sectionItemAction"
        to={item.to}
        {...item.props}
        key={item.id || item.to}
      >
        {item.text}
      </Action>
    ))}
  </ActionGroup>
)

describe('Section Item', () => {
  test('renders correctly', () => {
    const tree = create(<SectionItem title="Part one" />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  test('renders right component correctly ', () => {
    const { queryAllByTestId } = render(
      <SectionItem
        dotted
        rightComponent={actionComponent}
        title="Test Article"
      />,
    )

    expect(queryAllByTestId('sectionItemAction')).toHaveLength(items.length)
  })

  test('accepts custom component as a title', () => {
    const Wrapper = styled.div`
      display: inline-block;
    `

    const CustomTitle = (
      <Wrapper data-test-id="customTitleComponent">My important Book</Wrapper>
    )
    const { queryAllByTestId } = render(
      <SectionItem dotted title={CustomTitle} />,
    )

    expect(queryAllByTestId('customTitleComponent')).toBeTruthy()
  })
})

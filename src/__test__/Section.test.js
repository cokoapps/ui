import React from 'react'
import 'jest-styled-components'

import { Section } from '../ui'
import { create, render } from './_helpers'

/* eslint-disable-next-line react/prop-types */
const ListItemComponent = ({ id, name }) => (
  <li data-testid="listItem" id={id}>
    {name}
  </li>
)

describe('Section', () => {
  test('renders correctly', () => {
    const tree = create(
      <Section
        listItemComponent={ListItemComponent}
        listItems={[{ id: 1, name: 'test124' }]}
        title="Test Articles"
      />,
    ).toJSON()

    expect(tree).toMatchSnapshot()
  })

  test('renders right component correctly', () => {
    const testRightComponent = (
      <span data-testid="right">test right component</span>
    )

    const { queryByTestId } = render(
      <Section
        listItemComponent={ListItemComponent}
        listItems={[{ id: 1, name: 'test124' }]}
        rightComponent={testRightComponent}
        title="Test Articles"
      />,
    )

    expect(queryByTestId('right')).toBeTruthy()
  })

  test('renders list component with the right length ', () => {
    const data = [{ id: 1, name: 'test124' }, { id: 2, name: 'test124' }]

    const { queryAllByTestId } = render(
      <Section
        listItemComponent={ListItemComponent}
        listItems={data}
        title="Test Articles"
      />,
    )

    expect(queryAllByTestId('listItem')).toHaveLength(data.length)
  })

  test('displays message if list is empty', () => {
    const { queryAllByTestId } = render(
      <Section listItemComponent={ListItemComponent} title="Test Articles" />,
    )

    expect(queryAllByTestId('listItem')).toHaveLength(0)
    expect(queryAllByTestId('section-empty-message')).toHaveLength(1)
  })
})

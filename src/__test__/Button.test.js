import React from 'react'
import { fireEvent } from '@testing-library/react'
import 'jest-styled-components'
import theme from '@pubsweet/coko-theme'

import { Button } from '../ui'
import { create, render } from './_helpers'

describe('Button', () => {
  test('renders correctly', () => {
    const tree = create(<Button>Hello</Button>).toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('runs function on click', () => {
    const handleClick = jest.fn()
    const { getByText } = render(<Button onClick={handleClick}>Hello</Button>)

    expect(handleClick).toHaveBeenCalledTimes(0)
    fireEvent.click(getByText('Hello'))
    expect(handleClick).toHaveBeenCalledTimes(1)
  })

  test('Primary button to have primary class works', () => {
    const tree = create(<Button primary>Hi</Button>).toJSON()
    expect(tree).toHaveStyleRule('background', theme.colorPrimary)
  })

  test('Render icon correctly', () => {
    const { queryAllByTestId } = render(<Button icon="plus" primary />)
    expect(queryAllByTestId('iconButton')).toHaveLength(1)
  })
})

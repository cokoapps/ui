import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Button as UIButton, Icon as UIIcon } from '@pubsweet/ui'
import { override, th } from '@pubsweet/ui-toolkit'

const Icon = styled(UIIcon)`
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  height: ${th('fontSizeBase')};
  vertical-align: top;
  width: ${th('fontSizeBase')};
  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Button.Icon')}
`

const StyledButtonIcon = styled(UIButton)`
  border-color: ${props =>
    props.primary ? th('colorSecondary') : th('colorPrimary')};
  border-radius: 50%;
  border-style: ${th('borderStyle')};
  border-width: ${th('borderWidth')};
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  height: calc(${th('gridUnit')} * 6);
  line-height: calc(${th('gridUnit')} * 4);
  min-width: calc(${th('gridUnit')} * 6);
  padding: 0;
  text-align: center;
  vertical-align: top;

  &:hover {
    border-style: ${th('borderStyle')};
    border-width: ${th('borderWidth')};
  }
  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Button.Icon')}
`

/**
 * A component that can display a Button
 */
const Button = ({ children, icon, iconPosition, ...props }) =>
  children == null && icon !== null ? (
    <StyledButtonIcon {...props}>
      <Icon
        color={props.primary ? th('colorTextReverse') : th('colorPrimary')}
        data-testid="iconButton"
        {...props}
      >
        {icon}{' '}
      </Icon>
    </StyledButtonIcon>
  ) : (
    <UIButton {...props}>
      <span data-testid="iconButton">
        {icon && iconPosition === 'start' && (
          <Icon
            color={props.primary ? th('colorTextReverse') : th('colorPrimary')}
            data-testid="leftIconButton"
            primary={props.primary}
            {...props}
          >
            {icon}{' '}
          </Icon>
        )}
        <span>{children}</span>
        {icon && iconPosition === 'end' && (
          <Icon
            data-testid="rightIconButton"
            {...props}
            color={props.primary ? th('colorTextReverse') : th('colorPrimary')}
          >
            {icon}{' '}
          </Icon>
        )}
      </span>
    </UIButton>
  )

Button.propTypes = {
  /** The contents of the button (text, icon etc.) */
  children: PropTypes.node,
  /** ID to be used in test selectors */
  'data-test-id': PropTypes.string,
  /** Icon name (An icon name, from the Feather icon set.) */
  icon: PropTypes.string,
  /** Icon Position (Defines the position of the icon (if is is at the start of the button , or at the end)) */
  iconPosition: PropTypes.oneOf(['start', 'end']),
  /** Makes button a primary button */
  primary: PropTypes.bool,
}

Button.defaultProps = {
  children: null,
  'data-test-id': 'button',
  icon: null,
  iconPosition: 'start',
  primary: false,
}

export default Button

import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Button as UIButton, Icon as UIIcon } from '@pubsweet/ui'
import { override, th } from '@pubsweet/ui-toolkit'

const Icon = styled(UIIcon)`
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  height: ${th('fontSizeBase')};
  vertical-align: top;
  width: ${th('fontSizeBase')};

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Button.Icon')}
`
const ChevronIcon = styled(Icon)`
  float: right;
`

const StyledDropdown = styled.div`
  display: inline-block;
  position: relative;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Dropdown.Wrapper')}
`
const DropdownTitle = styled(UIButton)`
  cursor: pointer;
  display: inline-block;
  white-space: nowrap;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Dropdown.Title')}
`
const DropdownMenu = styled.ul`
  background-color: ${th('colorBackground')};
  border-color: ${th('colorBorder')};
  border-style: ${th('borderStyle')};
  border-width: ${th('borderWidth')};
  color: ${th('colorText')};
  display: ${props => (props.menuIsOpen ? 'block' : 'none')};
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  list-style-type: none;
  margin: calc(${th('gridUnit')} / 4) 0 0 0;
  padding: 0;
  position: absolute;
  top: 100%;
  width: 100%;
  z-index: 9;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Dropdown.Menu')}
`
const Item = styled.li`
  cursor: pointer;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  padding: ${th('gridUnit')};
  white-space: normal;
  word-break: break-word;

  &:hover {
    background-color: ${th('colorBackgroundHue')};
  }

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Dropdown.MenuItem')}
`

const Dropdown = ({ children, icon, iconPosition, itemsList, primary }) => {
  const [menuIsOpen, setMenuIsOpen] = useState(false)

  return (
    <StyledDropdown>
      <DropdownTitle
        onBlur={() => setMenuIsOpen(false)}
        onClick={() => setMenuIsOpen(!menuIsOpen)}
        primary={primary}
      >
        <span data-testid="iconButton">
          {icon && iconPosition === 'start' && (
            <Icon
              color={primary ? th('colorTextReverse') : th('colorPrimary')}
              data-testid="leftIconButton"
              primary={primary}
            >
              {icon}{' '}
            </Icon>
          )}

          <span>{children}</span>

          {icon && iconPosition === 'end' && (
            <Icon
              color={primary ? th('colorTextReverse') : th('colorPrimary')}
              data-testid="rightIconButton"
            >
              {icon}{' '}
            </Icon>
          )}
        </span>
        <ChevronIcon
          color={primary ? th('colorTextReverse') : th('colorPrimary')}
        >
          {!menuIsOpen ? 'chevron-down' : 'chevron-up'}
        </ChevronIcon>
      </DropdownTitle>
      <DropdownMenu menuIsOpen={menuIsOpen}>
        {itemsList.map(item => (
          <Item
            key={item.id}
            onClick={() => {
              item.onClick()
              setMenuIsOpen(false)
            }}
            {...item.props}
            onMouseDown={e => e.preventDefault()}
          >
            {item.title}
          </Item>
        ))}
      </DropdownMenu>
    </StyledDropdown>
  )
}

Dropdown.propTypes = {
  /** Content of the button of the dropdown */
  children: PropTypes.string.isRequired,
  /** Icon name (An icon name, from the Feather icon set.) */
  icon: PropTypes.string,
  /** Icon Position (Defines the position of the icon (if is is at the start of the button , or at the end)) */
  iconPosition: PropTypes.oneOf(['start', 'end']),

  /** List of the items for the drop down items */
  itemsList: PropTypes.arrayOf(
    PropTypes.shape({
      /** The key for the item  */
      id: PropTypes.string.isRequired,

      /** The click function for the item  */
      onClick: PropTypes.func.isRequired,

      /** The title for the item  */
      title: PropTypes.node.isRequired,
    }),
  ),

  /** Primary property for the dropdown, if it is false(or not set) then the dropdown will be secondary  */
  primary: PropTypes.bool,
}

Dropdown.defaultProps = {
  icon: null,
  iconPosition: 'start',
  itemsList: [],
  primary: false,
}

export default Dropdown

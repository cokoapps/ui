import React from 'react'
import PropTypes, { object } from 'prop-types'
import styled from 'styled-components'

import { List } from '@pubsweet/ui'
import { override } from '@pubsweet/ui-toolkit'

const Root = styled.div`
  display: flex;
  flex-direction: column;

  ${override('ui.Section')};
`

const Header = styled.div`
  display: flex;
  margin-bottom: ${props => props.theme.gridUnit};

  ${override('ui.Section.Header')};
`

const Title = styled.div`
  font-family: ${props => props.theme.fontInterface};
  font-size: ${props => props.theme.fontSizeHeading1};
  line-height: ${props => props.theme.lineHeightHeading1};
  text-transform: uppercase;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Section.Title')}
`

const StyledRightComponent = styled.div`
  flex-grow: 1;
  ${override('ui.Section.RightComponent')}
`

const StyledEmptyMessage = styled.div`
  font-family: ${props => props.theme.fontInterface};
  font-style: italic;
  padding: ${props => `calc(2* ${props.theme.gridUnit})`};
  text-align: center;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.Section.EmptyMessage')}
`

/**
 * A component that can display a list of items under a header.
 * Often useful in dashboards, but not limited to them.
 */
const Section = ({
  emptyListMessage,
  listItemComponent,
  listItems,
  rightComponent,
  title,
  ...props
}) => (
  <Root {...props}>
    <Header>
      <Title>{title}</Title>
      <StyledRightComponent>{rightComponent}</StyledRightComponent>
    </Header>

    {listItems.length === 0 && (
      <StyledEmptyMessage data-testid="section-empty-message">
        {emptyListMessage}
      </StyledEmptyMessage>
    )}

    {listItems.length > 0 && (
      <List component={listItemComponent} items={listItems} />
    )}
  </Root>
)

Section.propTypes = {
  /** Overrides default message when there are no items */
  emptyListMessage: PropTypes.string,
  /** The React component that pubsweet's List will render */
  listItemComponent: PropTypes.elementType.isRequired,
  /** The data that will be passed to pubsweet's List */
  // eslint-disable-next-line react/forbid-prop-types
  listItems: PropTypes.arrayOf(object),
  /** React component that can be displayed next to the title */
  rightComponent: PropTypes.element,
  /** Header title */
  title: PropTypes.string.isRequired,
}

Section.defaultProps = {
  emptyListMessage: 'There are no items in the list',
  listItems: [],
  rightComponent: null,
}

export default Section

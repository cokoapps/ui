import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { override, th } from '@pubsweet/ui-toolkit'

const Root = styled.div`
  align-items: center;
  display: flex;
  flex-basis: 100%;

  ${override('ui.SectionItem')};
`

const TitleContainer = styled.div`
  flex-grow: 1;
  overflow-x: hidden;
  overflow-y: hidden;
  padding: 0;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('ui.SectionItem.TitleContainer')}
`

const Title = styled.span`
  background-color: ${th('colorBackground')};
  color: ${th('colorText')};
  flex-grow: 0;
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
  overflow-y: hidden;
  padding-right: ${th('gridUnit')};
  word-wrap: break-word;

  &:after {
    content: ${props =>
      props.dotted
        ? `'. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '
      '. . . . . . . . . . . . . . . . . . . . '`
        : ''};
    flex-grow: 0;
    float: left;
    font-size: ${th('fontSizeBaseSmall')};
    padding-top: 3px;
    white-space: nowrap;
    width: 0;
  }

  /* stylelint-disable-next-line order/properties-alphabetical-order, order/order */
  ${override('ui.SectionItem.Title')}
`

const RightComponentWrapper = styled.div`
  display: flex;
  flex-shrink: 0;
  justify-content: flex-end;

  ${override('ui.SectionItem.RightComponent')}
`

/**
 * A component that can display an item inside a Section Component.
 * Often useful to display a list of articles or books.
 */
const SectionItem = ({ title, dotted, rightComponent }) => (
  <Root>
    <TitleContainer>
      <Title data-testid="sectionItemTitle" dotted={dotted}>
        {title}
      </Title>
    </TitleContainer>
    <RightComponentWrapper>{rightComponent}</RightComponentWrapper>
  </Root>
)

SectionItem.propTypes = {
  /** Defines if the item will have Dotts between title and actions */
  dotted: PropTypes.bool,
  /** A custom component usually for a list of actions for the section item , can be any type of component */
  rightComponent: PropTypes.node,
  /** Title of section Item */
  title: PropTypes.node.isRequired,
}

SectionItem.defaultProps = {
  dotted: false,
  rightComponent: null,
}

export default SectionItem

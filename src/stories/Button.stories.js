import React from 'react'

import { Button } from '../ui'

export const Primary = () => <Button primary>Hello th sdfgzsdfgdgsfis</Button>

export const PrimaryDisabled = () => (
  <Button disabled primary>
    Hello disabled
  </Button>
)
export const PrimaryWithIcon = () => (
  <Button icon="plus-circle" primary>
    Add
  </Button>
)

export const Secondary = () => <Button>Hello</Button>
export const SecondaryDisabled = () => <Button disabled>Hello</Button>
export const SecondaryWithIcon = () => (
  <Button icon="arrow_right" secondary>
    Hello
  </Button>
)

// When we pass only an icon parameter we get a round button
export const IconOnlyButtonPrimary = () => <Button icon="plus" primary />
export const IconOnlyButtonSecondary = () => <Button icon="plus" secondary />

export const IconAtTheEnd = () => (
  <Button icon="plus" iconPosition="end" primary>
    Add
  </Button>
)

export default { component: Button, title: 'Button' }

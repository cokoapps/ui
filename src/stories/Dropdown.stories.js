import React from 'react'

import { Dropdown } from '../ui'

const itemsList = [
  {
    id: 1,
    onClick: () => {},
    title: 'item 1n  asfasf asfkaskfh  ',
  },
  { id: 2, title: 'item 2' },
]

export const DropDownButtonPrimary = () => (
  <Dropdown itemsList={itemsList} primary>
    Test
  </Dropdown>
)
export const DropDownButtonSecondary = () => (
  <Dropdown itemsList={itemsList}>Test Test dsfsdgs dsgdsgdsg</Dropdown>
)

export const DropDownButtonPrimaryIcon = () => (
  <Dropdown icon="user" itemsList={itemsList} primary>
    Test
  </Dropdown>
)
export const DropDownButtonIconEnd = () => (
  <Dropdown icon="user" iconPosition="end" itemsList={itemsList} secondary>
    Test dsfsdgs dsgdsgdsg Test dsfsdgs dsgdsgdsg
  </Dropdown>
)

export default { component: Dropdown, title: 'Dropdown Button' }

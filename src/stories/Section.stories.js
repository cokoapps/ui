import React from 'react'
import styled from 'styled-components'

import { Action } from '@pubsweet/ui'

import { Section } from '../ui'

const ListItem = styled.li`
  background: gainsboro;
  border-radius: 5px;
  font-family: ${props => props.theme.fontInterface};
  list-style: none;
  margin-bottom: ${props => props.theme.gridUnit};
  padding: ${props => props.theme.gridUnit};
`

const StyledRightTestComponent = styled.div`
  display: flex;
  height: 100%;
  justify-content: flex-end;
`

const CustomComponentItem = item => <ListItem>{` ${item.name}`}</ListItem>

const items = [
  { id: 1, name: 'First item' },
  { id: 2, name: 'Second item' },
  { id: 3, name: 'Third item' },
]

const rightComponentTest = (
  <StyledRightTestComponent>
    <Action>I do something!</Action>
  </StyledRightTestComponent>
)

export const base = () => (
  <Section
    listItemComponent={CustomComponentItem}
    listItems={items}
    title="My articles"
  />
)

export const empty = () => (
  <Section
    listItemComponent={CustomComponentItem}
    title="Custom header title"
  />
)

export const emptyWithCustomMessage = () => (
  <Section
    emptyListMessage="I can say whatever I want"
    listItemComponent={CustomComponentItem}
    title="My articles"
  />
)

export const withRightComponent = () => (
  <Section
    listItemComponent={CustomComponentItem}
    listItems={items}
    rightComponent={rightComponentTest}
    title="My articles"
  />
)

export default {
  component: Section,
  title: 'Section',
}

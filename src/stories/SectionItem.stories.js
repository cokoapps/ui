import React from 'react'
import { Icon, Action, ActionGroup } from '@pubsweet/ui'
import faker from 'faker'
import styled from 'styled-components'

import { SectionItem } from '../ui'

const items = [{ id: 1, text: 'Edit' }, { id: 2, text: 'Delete' }]

const actionComponent = (
  <ActionGroup>
    {items.map(item => (
      <Action
        data-testid="sectionItemAction"
        to={item.to}
        {...item.props}
        key={item.id || item.to}
      >
        {item.text}
      </Action>
    ))}
  </ActionGroup>
)

const longBookTitle = faker.lorem.words(22)

export const base = () => (
  <SectionItem rightComponent={actionComponent} title="Part one" />
)

export const empty = () => <SectionItem title="Book Title" />

export const dotted = () => <SectionItem dotted title="My book title" />

export const longTitle = () => (
  <SectionItem dotted rightComponent={actionComponent} title={longBookTitle} />
)

const Title = styled.div`
  display: flex;
  padding-right: ${props => props.theme.unitGrid};
`

const StyledTitleText = styled.div`
  font-family: ${props => props.theme.fontHeading};
  justify-content: center;
`

const StyledTitleOptions = styled.span`
  color: ${props => props.theme.colorSecondary};
  font-size: ${props => props.theme.fontSizeBase};
  min-height: 100%;
`

const Wrapper = styled.div`
  display: inline-block;
`

const CustomTitle = (
  <Wrapper>
    <Title>
      <StyledTitleOptions>
        <Icon secondary>more_vertical</Icon> <Icon secondary>settings</Icon>
      </StyledTitleOptions>

      <StyledTitleText>My important Book</StyledTitleText>
    </Title>
  </Wrapper>
)

export const customTitleComponent = () => (
  <SectionItem dotted rightComponent={actionComponent} title={CustomTitle} />
)

export default {
  component: SectionItem,
  title: 'Section Item',
}
